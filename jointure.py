import duckdb

j1 = duckdb.sql("SELECT * FROM read_json_auto('films.json')")
j1.write_parquet("films.parquet")
# j1.show()
j2 = duckdb.sql("SELECT * FROM read_json_auto('actors.json')")
j2.write_parquet("actors.parquet")

p1 = duckdb.read_parquet("films.parquet")
p2 = duckdb.read_parquet("actors.parquet")
items = duckdb.sql("select * from p1 join p2 on p1.main_actor = p2.name")
# items = duckdb.sql("select * from p1 join p2 on p1.main_actor = p2.name").fetchall()
# for item in items:
#    print(item)
duckdb.sql("COPY items to 'jointure.json' (ARRAY TRUE)")
